package com.junit.app.nursesquickguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.junit.app.nursesquickguide.helpers.WordsListAdapter;

import java.util.ArrayList;

import static com.junit.app.nursesquickguide.MainActivity.AppWordsList;

public class HomeFragment extends Fragment {

    View rootView;
    AutoCompleteTextView atocmpWordsSearch;
    private WordsListAdapter mListVadapter;
    private InterstitialAd mInterstitialAd;
    private ArrayList<Pair<String, String>> mCustomWordsList;
    public HomeFragment() {
        // Required empty public constructor
        mCustomWordsList = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
ImageView imageView = (ImageView)rootView.findViewById(R.id.image);
        atocmpWordsSearch = (AutoCompleteTextView)rootView.findViewById(R.id.atocmptxtV_searchForWord);
        initWordsList();
        atocmpWordsSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                initWordsList();
                editListSearchable(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
imageView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getContext(), Main2Activity.class);
        startActivity(intent);
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-3517746418699749/3897599098");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getString(R.string.admob_interstetial_ad));
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });


    }
});

        ((ListView)rootView.findViewById(R.id.lstV_homeWordsList)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getContext(), ShowWordDetails.class);
                intent.putExtra("wordDetailsTitle", mCustomWordsList.get(i).first);
                intent.putStringArrayListExtra("wordDetailsArray",
                        AppWordsList.getmWordsList().get(mCustomWordsList.get(i).first).getmTraslatedWords());
                startActivity(intent);
                mInterstitialAd = new InterstitialAd(getContext());
                mInterstitialAd.setAdUnitId("ca-app-pub-3517746418699749/3897599098");
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                mInterstitialAd = new InterstitialAd(getContext());
                mInterstitialAd.setAdUnitId(getString(R.string.admob_interstetial_ad));
                AdRequest adRequest = new AdRequest.Builder().build();
                mInterstitialAd.loadAd(adRequest);
                mInterstitialAd.setAdListener(new AdListener() {
                    public void onAdLoaded() {
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        }
                    }
                });
            }
        });
        return rootView;
    }


    private void initWordsList() {
        mCustomWordsList = new ArrayList<>(AppWordsList.getmCustomWordList());
        mListVadapter = new WordsListAdapter(getContext(), mCustomWordsList);
        ((ListView)rootView.findViewById(R.id.lstV_homeWordsList)).setAdapter(mListVadapter);
    }

    public void editListSearchable(String searchWord) {
        ArrayList<Pair<String, String>> searchList = new ArrayList<>(mCustomWordsList);
        for(Pair<String, String> word : searchList)
            if(!word.first.contains(searchWord)) mCustomWordsList.remove(word);
        mListVadapter.notifyDataSetChanged();
    }

}
