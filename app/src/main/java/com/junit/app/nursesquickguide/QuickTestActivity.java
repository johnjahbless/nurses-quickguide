package com.junit.app.nursesquickguide;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.junit.app.nursesquickguide.helpers.WordDataBaseContract;
import com.junit.app.nursesquickguide.helpers.WordDataBaseHelper;
import com.junit.app.nursesquickguide.helpers.WordElement;
import com.junit.app.nursesquickguide.helpers.WordsList;

import java.util.ArrayList;
import java.util.Collections;

public class QuickTestActivity extends AppCompatActivity {

    public String mListName, tempWordMean, tempWordMeann;
    private WordsList mWordListForTest;
    public TextView textView, timeView;
    CountDownTimer countDownTimer;
    int count;
    private ArrayList<Pair<String, ArrayList<String>>> mWordListShuffled;
    private int pos = 0, listSize, correctAnswers = 0, Score = 0, counter;
    private InterstitialAd mInterstitialAd;
    AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queck_test);
        getSupportActionBar().hide();
        MobileAds.initialize(getApplicationContext(),
                "ca-app-pub-3517746418699749~1206914627");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequestt = new AdRequest.Builder().build();
        mAdView.loadAd(adRequestt);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3517746418699749/6499603907");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mListName = getIntent().getStringExtra("listName");
        textView = (TextView) findViewById(R.id.score);
        timeView = (TextView) findViewById(R.id.time);
        final Button hint = (Button) findViewById(R.id.btn_showWord);
        setTitle(mListName + " Test");
        getElementFromDB();
        initWordMap();
        startTest();
        textView.setText(String.valueOf("Score " + Score + "/" + listSize));
        ((Button) findViewById(R.id.btn_nextWord)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countDownTimer.cancel();
                ++pos;
                pos %= listSize;
                if (counter < 61) {
                    counter = 0;
                    startTest();

                }
                //countDownTimer.cancel();
            }
        });

        ((Button) findViewById(R.id.btn_previousWord)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                --pos;
                pos %= listSize;
                startTest();
            }
        });

        ((Button) findViewById(R.id.btn_resetWordsList)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = 0;
                startTest();
                counter = 0;
            }
        });

        ((Button) findViewById(R.id.btn_showWord)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (count <= 2) {
                    count++;
                    hint.setText("Hint" +"(" + count + "/3)");
                    Toast.makeText(getBaseContext(), "Correct Word : " + mWordListShuffled.get(pos).first
                            + " !", Toast.LENGTH_SHORT).show();
                }else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(QuickTestActivity.this);
                    alertDialogBuilder.setTitle("Purchase App");
                    alertDialogBuilder
                            .setMessage("Sorry, you have used all your lifelines, purchase the premium app to get unlimited lifelines to view hints")
                            .setCancelable(false)
                            .setPositiveButton(R.string.purchase,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setData(Uri.parse("market://details?id=com.junit.app.nursesquickguidepremium"));
                                            startActivity(intent);


                                        }
                                    })

                            .setNegativeButton(R.string.dismiss, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                //pos = 0;
                //startTest();
            }
        });

        ((Button) findViewById(R.id.btn_submitWords)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mWordListShuffled.get(pos).first.equals(((EditText) findViewById(R.id.edtxtV_userWordTest))
                        .getText().toString())) {
                    Score += 1;
                    textView.setText(String.valueOf("Score " + Score + "/" + listSize));
                    ((TextView) findViewById(R.id.txtV_wordStat)).setText("Correct !");
                    ((TextView) findViewById(R.id.txtV_wordStat)).setTextColor(getResources().getColor(android.R.color.holo_green_light));
                    ((EditText) findViewById(R.id.edtxtV_userWordTest)).setText("");
                    countDownTimer.cancel();
                    correctAnswers++;
                    ++pos;
                    pos %= listSize;
                    if (counter < 61) {
                        counter = 0;
                        startTest();

                    }
                } else if (((EditText) findViewById(R.id.edtxtV_userWordTest))
                        .getText().toString().equals("")) {
                    ((TextView) findViewById(R.id.txtV_wordStat)).setText("Please Enter Text !");
                } else {
                    ((TextView) findViewById(R.id.txtV_wordStat)).setText("Wrong !");
                    ((TextView) findViewById(R.id.txtV_wordStat)).setTextColor(getResources().getColor(android.R.color.holo_red_dark));

                }
            }
        });

    }
    @Override
    public void onBackPressed() {
        countDownTimer.cancel();
        counter = 0;
        super.onBackPressed();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            countDownTimer.cancel();
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    private void startTest() {
        just();
        if (pos == 0) ((Button) findViewById(R.id.btn_previousWord)).setEnabled(false);
        else ((Button) findViewById(R.id.btn_previousWord)).setEnabled(true);
        if (pos == mWordListShuffled.size())
            ((Button) findViewById(R.id.btn_nextWord)).setEnabled(false);
        else ((Button) findViewById(R.id.btn_nextWord)).setEnabled(true);

        ((EditText) findViewById(R.id.edtxtV_userWordTest)).setText("");
        tempWordMean = "";
        int translationSize = mWordListShuffled.get(pos).second.size();
        for (int j = 0; j < translationSize; j++) {
            tempWordMean += mWordListShuffled.get(pos).second.get(j);
            if (j != translationSize - 1)
                tempWordMean += "";
        }
        tempWordMean += "";
        if (tempWordMean.length() > 100) {

            tempWordMeann = tempWordMean.substring(0, 90);
            ((TextView) findViewById(R.id.txtV_wordMean)).setText("____" + tempWordMeann + ".....");

        } else {
            ((TextView) findViewById(R.id.txtV_wordMean)).setText("____" + tempWordMean + ".....");
        }
    }

    private void initWordMap() {
        mWordListShuffled = new ArrayList<>();
        for (WordElement word : mWordListForTest.getmWordsList().values()) {
            mWordListShuffled.add(Pair.create(word.getmSourceEnglishWord(), word.getmTraslatedWords()));
        }
        Collections.shuffle(mWordListShuffled);
        listSize = mWordListShuffled.size();
    }


    private void getElementFromDB() {
        WordDataBaseHelper mDbHelper = new WordDataBaseHelper(this);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] returnedColumns = {WordDataBaseContract.WordDataBaseEntry.COLUMN_LIST_ELEMENT};
        String selection = WordDataBaseContract.WordDataBaseEntry.COLUMN_LIST_NAME + " = ?";
        String[] selectionArgs = {mListName};
        Cursor cursor = db.query(WordDataBaseContract.WordDataBaseEntry.TABLE_NAME,
                returnedColumns,
                selection,
                selectionArgs,
                null, null, null);
        cursor.moveToFirst();
        String json = cursor.getString(cursor.getColumnIndexOrThrow(
                WordDataBaseContract.WordDataBaseEntry.COLUMN_LIST_ELEMENT));
        cursor.close();
        Gson gson = new Gson();
        mWordListForTest = gson.fromJson(json, WordsList.class);

    }

    public void just() {

      countDownTimer = new CountDownTimer(61000, 1000) {
            public void onTick(long millisUntilFinished) {
                counter++;
                timeView.setText(String.valueOf(counter + "s"));

            }


            public void onFinish() {

                counter = 0;

                timeView.setText(String.valueOf(counter + "s"));

                Toast.makeText(getBaseContext(),
                      "Your time is up", Toast.LENGTH_SHORT).show();
                ++pos;
                pos %= listSize;
                startTest();




            }

        }.start();
    }

    public void back(View view) {
        onBackPressed();
        countDownTimer.cancel();
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId(getString(R.string.admob_interstitial_ad1));
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });
    }
}
